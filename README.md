# manage-users
Manage users in CVCloud.

## Usage

```shell
$ helm repo add manage-cks-users https://gitlab.com/api/v4/projects/23604962/packages/helm/stable
$ helm repo update
$ helm install manage-cks-users manage-cks-users/manage-cks-users --version <VERSION>
```
